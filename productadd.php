<?php
 include_once 'includes/dbh.php';
?>


<!DOCTYPE html>
<html>
<head>
    <title>PRODUCT ADD</title>
    <meta charset="utf-8">
    <link rel="stylesheet" type="text/css" href="css/style.css">
</head>
    <body> 
    <header>
        <nav>
        <h1>PRODUCT ADD</h1>
        <ul>
            <li><a href="index.php">PRODUCT LIST</a></li>
        </ul>
        </nav>
        </header>

<form action="includes/dbaddproduct.php" method="POST"> 
<label for="sku">SKU: <input type="text" name="sku" placeholder="product sku" required></label>       

<br>
<label for="name">Name: <input type="text" name="name" placeholder="product name" required></label>
<br>
<label for="price">Price: <input type="number" name="price" step="0.01" min="0.01" placeholder="product price" required></label>
<br>
<label for="options">Type: <select required name="options" id="menu" onchange="choise()">
  <option hidden value="">type switcher</option>
  <option value="one">Size</option>
  <option value="two">Dimensions</option>
  <option value="three">Weight</option>
</select>
</label>
<input id="size" type="number" name="size" step="0.01" min="0.01" placeholder="product size" hidden>
<input id="height" type="number" name="height" step="0.01" min="0.01" placeholder="product height" hidden>
<input id="width" type="number" name="width" step="0.01" min="0.01" placeholder="product width" hidden>
<input id="lenght" type="number" name="lenght" step="0.01" min="0.01" placeholder="product lenght" hidden>
<input id="weight" type="number" name="weight" step="0.01" min="0.01" placeholder="product weight" hidden>
<p id="spara" hidden>Please input size in megabytes (MB)!</p>
<p id="dpara" hidden>Please input dimensions in height, width and lenght (HxWxL)!</p>
<p id="wpara" hidden>Please input weight in kilograms (KG)!</p>

<button id="submitbtn2" type="submit" name="submit">SAVE</button>

        </form>       
        </body>
        </html>


<script>
var menu = document.getElementById("menu"),
   selection = [
      document.getElementById("size"),
      document.getElementById("height"),
      document.getElementById("width"),
      document.getElementById("lenght"),
      document.getElementById("weight"),
      document.getElementById("spara"),
      document.getElementById("dpara"),
      document.getElementById("wpara")

   ];

function choise() {
for (i=0; i<selection.length; i++){
    if(menu.value === "default"){ 
       selection[i].style.display = "none";
     } else if(menu.value === "one"){
       selection[i].style.display = "none";
       selection[0].style.display = "block";
       size.setAttribute("required", "");
       selection[5].style.display = "block";
      } else if(menu.value === "two"){
       selection[i].style.display = "none";
       selection[1].style.display = "block";
       selection[2].style.display = "block";
       selection[3].style.display = "block";
       height.setAttribute("required", "");
       width.setAttribute("required", "");
       lenght.setAttribute("required", "");
       selection[6].style.display = "block";
      } else if(menu.value === "three"){
       selection[i].style.display = "none";
       selection[4].style.display = "block";
       weight.setAttribute("required", "");
       selection[7].style.display = "block";
      }
 }
}



</script>